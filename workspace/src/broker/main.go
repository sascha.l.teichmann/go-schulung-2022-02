package main

import (
	"fmt"
	"time"
)

type broker struct {
	messages    chan string
	connections chan *client
	clients     []*client
}

type client struct {
}

func newBroker() *broker {

	return &broker{
		messages:    make(chan string),
		connections: make(chan *client),
	}
}

func (b *broker) run() {
	for {
		select {
		case msg := <-b.messages:
			fmt.Println("From broker:", msg)
			for _, c := range b.clients {
				c.receive(msg)
			}
		case c := <-b.connections:
			fmt.Println("Registriert")
			b.clients = append(b.clients, c)
		}

	}
}

func (b *broker) connect() *client {
	c := &client{}
	b.connections <- c
	return c
}

func (b *broker) send(msg string) {
	b.messages <- msg
}

func (c *client) run(b *broker) {
	for {
		b.send("Hallo")
		time.Sleep(time.Second * 2)
	}
}

func (c *client) receive(msg string) {
	fmt.Println("receive in client:", msg)
}

func main() {

	b := newBroker()

	go b.run()

	for i := 0; i < 5; i++ {

		c := b.connect()
		go c.run(b)
	}

	select {}
}
