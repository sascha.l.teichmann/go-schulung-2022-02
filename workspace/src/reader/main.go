package main

import (
	"io"
	"log"
	"os"
)

type UpperCase struct {
	io.Reader
}

func (uc UpperCase) Read(b []byte) (int, error) {
	n, err := uc.Reader.Read(b)
	for i, v := range b[:n] {
		if v == 'a' {
			b[i] = 'A'
		}
	}
	return n, err
}

func main() {
	if _, err := io.Copy(os.Stdout, UpperCase{os.Stdin}); err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
