package main

import "fmt"

func main() {

	sl := make([]int, 2, 3)

	sl[0] = 56

	fmt.Printf("%v\n", sl)

	sl = append(sl, 32)
	fmt.Printf("%v\n", sl)
	sl = append(sl, 64)
	fmt.Printf("%v\n", sl)
}
