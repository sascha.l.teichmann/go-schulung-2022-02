package main

import "fmt"

func nummerEins(x, y int) (int, int) { // call by value
	x = 13
	fmt.Println(x)

	return x * y, 42
}

func nummerZwei() (a int, b int) {

	a, b = 2, 4

	a, b = b, a

	return
}

func topLevel(a, b int) int {

	nested := func(x, y int) int {

		return a * x * b * y
	}

	return nested(3, 4)
}

func factory(x int) func() {
	return func() {
		fmt.Println("inner", x)
	}
}

func main() {
	var x int
	x = 21
	var f float32 = 13.45

	var z int8

	nummerEins(int(f), 43)
	w, _ := nummerEins(int(z), 56)
	fmt.Println(x)
	fmt.Println(w)
	a, b := nummerZwei()
	fmt.Println(a, b)

	var fn func() = factory(42)
	fn2 := factory(32)
	fmt.Println("hallo")
	fn()
	fn2()

	fmt.Println(topLevel(1, 2))

}
