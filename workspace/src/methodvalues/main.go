package main

import "fmt"

type Time string

func (t Time) Print() {
	fmt.Println("Time:", t)
}

func doIt(fn func()) {
	fmt.Println("before")
	fn()
	fmt.Println("after")
}

func work() {
	fmt.Println("Hello from work")
}

func freetime() {
	fmt.Println("Hello from freetime")
}

func main() {
	doIt(work)
	doIt(freetime)

	// var t Time = "Hallo"
	t := Time("Hallo")
	t.Print()

	// Method value

	doIt(t.Print)
	// doIt(func() { t.Print() })
}
