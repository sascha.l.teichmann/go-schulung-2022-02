package main

import "fmt"

type Auto interface {
	Bremsen()
	Anfahren()
}

func fahren(a Auto) {
	a.Anfahren()
	a.Bremsen()
}

type Bremse rune

func (b Bremse) Bremsen() {
	fmt.Println("bremsen")
}

type Kupplung string

func (k Kupplung) Anfahren() {
	fmt.Println("kuppeln")
}

func main() {

	var auto struct {
		Bremse
		Kupplung
	}

	fahren(auto)
}
