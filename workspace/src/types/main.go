package main

import "fmt"

type DasIstMeinTyp int // unlying type/kind (physikalische Repräsention)

func (dimt DasIstMeinTyp) Hallo() {
	fmt.Println(dimt)
}

func (dimt DasIstMeinTyp) Hallo2() {
}

// Pointer Receiver
func (dimt *DasIstMeinTyp) modify() {
	if dimt == nil {
		return
	}
	*dimt = 42
}

func main() {

	var mt DasIstMeinTyp

	mt = 32

	mt.Hallo()
	mt.modify()
	// (&mt).modify()
	fmt.Println(mt)

	var mpt *DasIstMeinTyp // = nil

	mpt.modify()
}
