package main

import "fmt"

func calc(arr [10]int) {

	for _, v := range arr {
		fmt.Printf("%d\n", v)
	}
}

func main() {

	barry := [...]int{
		5: 42,
		7: 65,
	}

	fmt.Println(barry)

	var array [10]int

	array[3] = 2

	calc(array)

	fmt.Println(len(array))

	array[0] = 10
	x := 10
	array[x] = 32
	fmt.Println(array)
}
