package greet

import "fmt"

func init() {
	fmt.Println("init 1")
	fmt.Println("AAA")
}

func init() {
	fmt.Println("init 2")
}

func hello() {
	fmt.Println("Mein erstes Paket")
}

func Hello() {
	hello()
}
