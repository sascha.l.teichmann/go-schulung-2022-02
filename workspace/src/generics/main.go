package main

import (
	"fmt"

	"golang.org/x/exp/constraints"
)

type OrderedList[T any] struct {
	next  *OrderedList[T]
	value T
}

func minInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func minFloat(a, b float64) float64 {
	if a < b {
		return a
	}
	return b
}

type number interface {
	int | int32 | string | float64
}

func min[T constraints.Ordered](a, b T) T {
	if a < b {
		return a
	}
	return b
}

func main() {

	x := min(1.23, 3.67)
	fmt.Println(x)

	m := minInt(230, -1)
	fmt.Println(m)

	n := minFloat(230.2367, -1.89)
	fmt.Println(n)
}
