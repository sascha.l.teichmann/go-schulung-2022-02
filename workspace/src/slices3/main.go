package main

import "fmt"

func grow(sl *[]int) {
	*sl = append(*sl, 1, 2, 3, 4, 5)
	fmt.Println(*sl)
}

func main() {

	sl := make([]int, 0, 3)
	grow(&sl)

	fmt.Println(sl)
}
