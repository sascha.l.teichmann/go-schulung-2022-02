package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"time"
)

type datum struct {
	time.Time
}

type Buch struct {
	ErscheinungsJahr datum
	Vorwort          string `json:"preface" xml:"vorwort"`
	Seiten           []string
	Nachwort         string
}

type Essay struct {
	Seiten []string
}

func (d datum) MarshalJSON() ([]byte, error) {
	return []byte(`"` + d.Format(time.RFC3339) + `"`), nil
}

func main() {

	b := Buch{
		ErscheinungsJahr: datum{time.Now()},
		Vorwort:          "Es war einmal...",
		Seiten: []string{
			"Erstes Kapitel",
			"Zweites Kapitel",
		},
		Nachwort: "Und die Moral ...",
	}

	var buf bytes.Buffer

	enc := json.NewEncoder(&buf)
	enc.SetIndent("", "  ")

	if err := enc.Encode(&b); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	fmt.Println(buf.String())

	dec := json.NewDecoder(bytes.NewReader(buf.Bytes()))

	var zweite Essay

	if err := dec.Decode(&zweite); err != nil {
		log.Fatalf("error: %v\n", err)
	}

	for i, p := range zweite.Seiten {
		fmt.Printf("%d: %s\n", i+1, p)

	}

	/*

		adHoc := map[string]any{
			"klaus": 42,
			"petra": []string{"Hallo", "Welt"},
		}

		if err := enc.Encode(adHoc); err != nil {
			log.Fatalf("error: %v\n", err)
		}

	*/

	/*

		xenc := xml.NewEncoder(os.Stdout)
		if err := xenc.Encode(&b); err != nil {
			log.Fatalf("error: %v\n", err)
		}
	*/

}
