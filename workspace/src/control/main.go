package main

import "fmt"

func f() int {
	return 42
}

func main() {

	{
	}

	if x := f(); x == 3 {
		// x sichtbar
	} else if x == 4 {
		// x sichtbar
	} else {
		// x sichtbar
	}

	for x := 0; x <= 10; x += 1 {
	}

	// while (cond)
	for x < 10 {
	}

	// for (;;)
	for {
	}

	for ; x < 100; x++ {
	}

	// do ... while
	for {
		// ..
		if !cond {
			break
		}
	}

exit:
	for {
		for {
			for {
				if cond {
					break exit // continue
				}
			}
		}
	}

	for {
		goto end
	}

end:

	switch zaehler := f(); zaehler {
	case 0, 1:
		fallthrough
	case 2: //
	default: //
	}

	switch {
	case x < 10, y < 10:
		fmt.Println()
		fmt.Println()
	case x < 100:
	default:
	}
}
