package main

import (
	"errors"
	"fmt"
	"image"
)

type printer interface {
	Print()
}

type Printable int

func (p Printable) Print() {
	fmt.Println("Hello")
}

func (p Printable) String() string {
	return "xxxx"
}

func myprint(args ...interface{}) {

	for _, v := range args {

		// x := v.(int) // type assertion

		if x, ok := v.(int); ok {
			x += 1
			fmt.Println(v, x)
		}

		// type switch

		switch x := v.(type) {
		case int:
			fmt.Println("int", x)
		case float64:
			fmt.Println("float64", x)
		case printer:
			x.Print()
		default:
			fmt.Println("unknown")
		}
	}
}

func subImage(img image.Image, box image.Rectangle) (image.Image, error) {
	if sub, ok := img.(interface {
		SubImage(image.Rectangle) image.Image
	}); ok {
		return sub.SubImage(box), nil
	}
	return nil, errors.New("No subimage")
}

func main() {
	var p Printable
	fmt.Println(p)
	myprint(1, 3.1415, "string", p)
	//myprint(1)
}
