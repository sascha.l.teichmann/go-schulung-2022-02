package main

import "fmt"

const large = 1e20

const (
	one = iota * iota
	two
	three
)

type aufzaehlung int

const (
	erster aufzaehlung = iota
	zweiter
	dritter
)

func (a aufzaehlung) Valid() bool {
	return erster <= a && a <= dritter
}

func main() {

	var x int32

	x = two

	fmt.Println(one)
	fmt.Println(x)
	fmt.Println(three)

	a := aufzaehlung(4)
	b := dritter

	fmt.Println(a.Valid())
	fmt.Println(b.Valid())
}
