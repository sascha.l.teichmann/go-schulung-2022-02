package main

import "fmt"

func main() {
	//var m map[string]int

	//m := make(map[string]int)
	m := map[string]int{
		"Klaus":  10,
		"Jürgen": 0,
	}

	m["Petra"] = 3

	fmt.Println(m["Klaus"])
	fmt.Println(m["Petra"])
	fmt.Println(m["Otto"])
	fmt.Println(m["Jürgen"])

	if v, ok := m["Otto"]; ok {
		fmt.Printf("Otto: %d\n", v)
	}
	if v, ok := m["Jürgen"]; ok {
		fmt.Printf("Jürgen: %d\n", v)
	}

	for k, v := range m {
		fmt.Printf("%s: %d\n", k, v)
	}
}
