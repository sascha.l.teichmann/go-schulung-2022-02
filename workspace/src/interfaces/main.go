package main

import "fmt"

type Auto interface {
	Anfahren()
	Bremsen()
}

func fahren(a Auto) {
	a.Anfahren()
	a.Bremsen()
}

type AllInOne int

func (aio AllInOne) Anfahren() {
	fmt.Println("AllInOne: Anfahren")
}

func (aio AllInOne) Bremsen() {
	fmt.Println("AllInOne: Bremsen")
}

func main() {
	var a AllInOne

	fahren(a)
}
