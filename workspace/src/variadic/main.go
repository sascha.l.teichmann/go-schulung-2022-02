package main

import "fmt"

func myprint(sl ...int) {
	for _, v := range sl {
		fmt.Println(v)
	}
}

func main() {
	fmt.Println("vim-go", 2389, 23892, 290)
	x := []int{2, 3, 4}
	myprint(x...)
	myprint(1, 12, 23, 78)
}
