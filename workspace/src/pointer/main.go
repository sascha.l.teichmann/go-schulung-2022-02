package main

import "fmt"

func calc(x *int) {
	fmt.Println(*x)
	*x = 42
}

func create() *int {
	var x int = 78
	return &x
}

func main() {

	//mp := new(int)

	var y int = 32

	var yp *int

	yp = &y

	calc(yp)
	fmt.Println(y)

	xp := create()
	fmt.Println(*xp)

}
