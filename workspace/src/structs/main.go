package main

import "fmt"

type lieblingsStruktur struct {
	a float32
	b bool
	c int
	d [18]complex64
	_ [120]byte
}

func main() {

	st := lieblingsStruktur{
		a: 3.1415,
	}

	st.b = false
	st.c = 42

	fmt.Printf("%v\n", st)

}
