package main

import "fmt"

type MeinType struct {
	a int
}

func (mt MeinType) basisMethode() {
	fmt.Println("MT: basisMethode")
}

type IhrType float32

func (it IhrType) basisMethode() {
	fmt.Println("IT: basisMethode")
}

type DeinType struct {
	MeinType
	*IhrType
}

func (dt DeinType) basisMethode() {
	fmt.Println("DT: derivedMethode", dt.a)
	dt.MeinType.basisMethode()
}

func main() {

	var (
		mt MeinType
		dt DeinType
	)

	mt.basisMethode()
	dt.basisMethode()
	//dt.derivedMethod()
}
