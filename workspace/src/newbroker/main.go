package main

import (
	"fmt"
	"time"
)

type broker struct {
	commands chan func(*broker)
	clients  []*client
}

type client struct {
}

func newBroker() *broker {

	return &broker{
		commands: make(chan func(*broker)),
	}
}

func (b *broker) run() {
	for cmd := range b.commands {
		cmd(b)
	}
}

func (b *broker) connect() *client {
	c := &client{}
	b.commands <- func(b *broker) {
		b.clients = append(b.clients, c)
	}
	return c
}

func (b *broker) send(msg string) {
	done := make(chan struct{})
	b.commands <- func(b *broker) {
		defer close(done)
		fmt.Println("From broker:", msg)
		for _, c := range b.clients {
			c.receive(msg)
		}
	}
	<-done
	fmt.Println("message sent")
}

func (c *client) run(b *broker) {
	for {
		b.send("Hallo")
		time.Sleep(time.Second * 2)
	}
}

func (c *client) receive(msg string) {
	fmt.Println("receive in client:", msg)
}

func main() {

	b := newBroker()

	go b.run()

	for i := 0; i < 5; i++ {

		c := b.connect()
		go c.run(b)
	}

	select {}
}
