package main

import "fmt"

func main() {
	// var s string
	s := `vim-


göo` + "Hallo"

	fmt.Printf("len: %d\n", len(s))
	fmt.Printf("rune: %c\n", s[5])

	//s[1] = 'A'

	for i, c := range s {
		fmt.Printf("%d: %c\n", i, c)
	}

	runes := []rune(s)
	fmt.Printf("len: %d\n", len(runes))

	for i, c := range runes {
		fmt.Printf("%d: %c\n", i, c)
	}
}
