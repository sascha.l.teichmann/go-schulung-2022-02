package main

import (
	"errors"
	"flag"
	"fmt"
	"log"
	"regexp"
)

var parserRe = regexp.MustCompile("(abc)")

func recursive(i int) {
	if i > 0 {
		recursive(i - 1)
	}

	if i == 23 {
		panic("Buuh!")
	}
	fmt.Println(i)
}

func calculate(input int) (n int, err error) {

	defer func() {
		if x := recover(); x != nil {
			err = fmt.Errorf("böse: %v", x)
			return
		}

		fmt.Println("Hallo von defer")
	}()

	defer func() {
		fmt.Println("Hallo von defer 2")
	}()

	recursive(50)

	if input == 32 {
		return 0, errors.New("Falsche Eingabe")
	}

	return 42, nil
}

func main() {

	input := flag.Int("input", 42, "Das ist die Eingabe")
	flag.Parse()

	// Err-lang

	if v, err := calculate(*input); err != nil {
		log.Fatalf("error: %v\n", err)
	} else {
		fmt.Println(v)
	}
}
