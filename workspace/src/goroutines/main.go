package main

import (
	"fmt"
	"sync"
)

func main() {

	//var ch chan bool

	//done := make(chan struct{})

	messages := make(chan int)

	var wg sync.WaitGroup

	for i := 1; i <= 100; i++ {
		wg.Add(1)

		go func(i int) {
			//defer func() { done <- struct{}{} }()
			//defer close(done)
			defer wg.Done()

			for msg := range messages {
				fmt.Println(i, msg)
			}
		}(i)
	}

	for i := 0; i < 1000; i++ {
		messages <- i
	}

	close(messages)

	wg.Wait()

	//<-done
	//_, ok := <-done
	//fmt.Println(ok)

}
