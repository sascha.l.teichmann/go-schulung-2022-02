package main

import "fmt"

func calc(sl []int) {
	for _, v := range sl {
		fmt.Println(v)
	}
}

func main() {

	arr := [10]int{}

	var sl []int

	if sl == nil {
		fmt.Println("its nil", len(sl))
	}

	sl = arr[0:9]
	sl2 := arr[2:8]
	sl2[0] = 67
	fmt.Println(len(sl))
	fmt.Println("cap: ", cap(sl))
	//sl = sl[:len(sl)+10]
	calc(sl)
	calc(sl[:len(sl)-1])

	fmt.Printf("%v\n", arr)
	fmt.Printf("%v\n", sl)

}
