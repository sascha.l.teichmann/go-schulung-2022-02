package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
)

type broker struct {
	commands chan func(*broker)
	clients  map[*client]struct{}
}

type client struct {
	conn net.Conn
	out  chan string
}

func newBroker() *broker {

	return &broker{
		commands: make(chan func(*broker)),
		clients:  make(map[*client]struct{}),
	}
}

func (b *broker) run() {
	for cmd := range b.commands {
		cmd(b)
	}
}

func newClient(conn net.Conn) *client {
	return &client{
		conn: conn,
		out:  make(chan string, 5),
	}
}

func (b *broker) connect(c *client) {
	b.commands <- func(b *broker) {
		b.clients[c] = struct{}{}
	}
}

func (b *broker) disconnect(c *client) {
	b.commands <- func(b *broker) {
		delete(b.clients, c)
	}
}

func (b *broker) send(c *client, msg string) {

	b.commands <- func(b *broker) {
		//fmt.Println("From broker:", msg)
		for x := range b.clients {
			if x == c {
				continue
			}
			if err := x.receive(msg); err != nil {
				log.Printf("receive error: %v\n", err)
				delete(b.clients, x)
				// TODO: kill client
			}
		}
	}
}

func (c *client) run(b *broker) {

	done := make(chan struct{})

	defer func() {
		close(done)
		b.disconnect(c)
		c.conn.Close()
	}()

	go func() {
		for {
			select {
			case msg := <-c.out:
				_, err := fmt.Fprintf(c.conn, "%s\n", msg)
				if err != nil {
					log.Printf("error: %v\n", err)
					return
				}
			case <-done:
				return
			}
		}
	}()

	sc := bufio.NewScanner(c.conn)

	for sc.Scan() {
		msg := sc.Text()
		b.send(c, msg)
	}

	if err := sc.Err(); err != nil {
		log.Printf("client read error: %v\n", err)
	}
}

var errTooSlow = errors.New("client too slow")

func (c *client) receive(msg string) error {
	c.out <- msg
	return nil

	/*
		select {
		case c.out <- msg:
		default:
			return errTooSlow
		}
		return nil
	*/
}

type server struct {
	addr   string
	port   int
	broker *broker
}

func newServer(addr string, port int, broker *broker) *server {
	return &server{
		addr:   addr,
		port:   port,
		broker: broker,
	}
}

func (s *server) run(errors chan error) {

	l, err := net.Listen("tcp", fmt.Sprintf("%s:%d", s.addr, s.port))
	if err != nil {
		errors <- err
		return
	}

	for {
		conn, err := l.Accept()
		if err != nil {
			log.Printf("connecting client error: %v\n", err)
			continue
		}

		c := newClient(conn)

		s.broker.connect(c)

		go c.run(s.broker)
	}

	close(errors)
}

func main() {

	addr := flag.String("addr", "localhost", "binding interface")
	port := flag.Int("port", 12345, "port")

	flag.Parse()

	b := newBroker()
	go b.run()

	s := newServer(*addr, *port, b)

	serverErrors := make(chan error)

	go s.run(serverErrors)

	if err := <-serverErrors; err != nil {
		log.Fatalf("error: %v\n", err)
	}
}
