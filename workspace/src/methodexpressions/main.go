package main

import "fmt"

type MeinTyp int

func (mt MeinTyp) foo() {
	fmt.Println("foo")
}

func main() {

	var m MeinTyp

	m.foo()

	// Method expressions

	var fn func(MeinTyp)

	fn = MeinTyp.foo

	fn(m)
}
